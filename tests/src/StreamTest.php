<?php

/**
 * This file is part of pdfversion.
 *
 * (c) TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\Tests;

use Trekksoft\PdfVersion\Stream;
use Trekksoft\PdfVersion\VersionDiscovery\HeaderCommentDiscoverer;

class StreamTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider pdfPaths
     *
     * @param string $path
     */
    public function header_comment_discoverer_should_work_with_enough_data($path)
    {
        $discoverer = new HeaderCommentDiscoverer();

        $fileh = fopen($path, 'r');
        $tmph = fopen('php://memory', 'r+');

        stream_copy_to_stream($fileh, $tmph, 10); // 10 bytes is enough

        $version = basename(dirname($path));
        $stream = new Stream($tmph);
        $this->assertSame($version, $discoverer->getVersion($stream)->toString());
    }

    /**
     * @test
     * @dataProvider pdfPaths
     * @expectedException \Trekksoft\PdfVersion\VersionDiscovery\UnknownVersionException
     *
     * @param string $path
     */
    public function header_comment_discoverer_should_fail_with_too_few_data($path)
    {
        $discoverer = new HeaderCommentDiscoverer();

        $fileh = fopen($path, 'r');
        $tmph = fopen('php://memory', 'r+');

        stream_copy_to_stream($fileh, $tmph, 5); // 5 bytes is not enough

        $stream = new Stream($tmph);
        $discoverer->getVersion($stream)->toString();
    }

    /**
     * @return string[]
     */
    public function pdfPaths()
    {
        $tests = [];
        foreach (glob(__DIR__.'/../files/*/*.pdf') as $path) {
            $tests[] = [realpath($path)];
        }

        return $tests;
    }
}
