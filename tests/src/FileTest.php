<?php

/**
 * This file is part of pdfversion.
 *
 * (c) TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\Tests;

use Trekksoft\PdfVersion\File;
use Trekksoft\PdfVersion\VersionDiscovery\DiscovererChain;
use Trekksoft\PdfVersion\VersionDiscovery\HeaderCommentDiscoverer;

class FileTest extends \PHPUnit_Framework_TestCase
{
    /** @var DiscovererChain */
    private $discovererChain;

    public function setUp()
    {
        $this->discovererChain = new DiscovererChain([
            new HeaderCommentDiscoverer(),
        ]);
    }

    /**
     * @test
     * @dataProvider pdfPaths
     *
     * @param string $path
     */
    public function test_pdf_file($path)
    {
        $version = basename(dirname($path));
        $file = new File($path);
        $this->assertSame($version, $this->discovererChain->getVersion($file)->toString());
    }

    /**
     * @return string[]
     */
    public function pdfPaths()
    {
        $tests = [];
        foreach (glob(__DIR__.'/../files/*/*.pdf') as $path) {
            $tests[] = [realpath($path)];
        }

        return $tests;
    }
}
