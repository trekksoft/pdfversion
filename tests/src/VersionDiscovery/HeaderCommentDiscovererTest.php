<?php

/**
 * This file is part of pdfversion.
 *
 * (c) TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\Tests\VersionDiscovery;

use Trekksoft\PdfVersion\Stream;
use Trekksoft\PdfVersion\VersionDiscovery\HeaderCommentDiscoverer;

class HeaderCommentDiscovererTest extends \PHPUnit_Framework_TestCase
{
    /** @var  HeaderCommentDiscoverer */
    private $discoverer;

    public function setUp()
    {
        $this->discoverer = new HeaderCommentDiscoverer();
    }

    public function testHeaders()
    {
        $this->assertSame('1.2', $this->discoverer->getVersion($this->createStream('%PDF-1.2'))->toString());
        $this->assertSame('1.3', $this->discoverer->getVersion($this->createStream('%PDF-1.3'))->toString());
        $this->assertSame('1.4', $this->discoverer->getVersion($this->createStream('%PDF-1.4'))->toString());
        $this->assertSame('1.5', $this->discoverer->getVersion($this->createStream('%PDF-1.5'))->toString());
    }

    /**
     * @expectedException \Trekksoft\PdfVersion\VersionDiscovery\UnknownVersionException
     * @expectedExceptionMessage Failed to find '%PDF-X.Y' header
     */
    public function testInvalidHeaders()
    {
        $this->discoverer->getVersion($this->createStream('foo'));
    }

    /**
     * @param string $text
     *
     * @return Stream
     */
    private function createStream($text)
    {
        $fh = fopen('php://temp', 'rw');
        fwrite($fh, $text);

        return new Stream($fh);
    }
}
