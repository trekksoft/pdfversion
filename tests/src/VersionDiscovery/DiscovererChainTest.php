<?php

/**
 * This file is part of pdfversion.
 *
 * (c) TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\Tests\VersionDiscovery;

use Trekksoft\PdfVersion\Stream;
use Trekksoft\PdfVersion\VersionDiscovery\ChainableDiscoverer;
use Trekksoft\PdfVersion\VersionDiscovery\DiscovererChain;
use Trekksoft\PdfVersion\VersionDiscovery\UnknownVersionException;
use Trekksoft\PdfVersion\VersionDiscovery\Version;

class DiscovererChainTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function first_discoverer_succeeds()
    {
        $chain = new DiscovererChain();
        $chain->addDiscoverer(new SucceedingDiscoverer(new Version(1, 0)));
        $chain->addDiscoverer(new SucceedingDiscoverer(new Version(1, 1)));

        $stream = $this->createEmptyStream();

        $this->assertEquals(new Version(1, 0), $chain->getVersion($stream));
    }

    /** @test */
    public function first_discoverer_fails_but_second_discoverer_succeeds()
    {
        $chain = new DiscovererChain();
        $chain->addDiscoverer(new FailingDiscoverer());
        $chain->addDiscoverer(new SucceedingDiscoverer(new Version(1, 1)));

        $stream = $this->createEmptyStream();

        $this->assertEquals(new Version(1, 1), $chain->getVersion($stream));
    }

    /**
     * @test
     * @expectedException \Trekksoft\PdfVersion\VersionDiscovery\UnknownVersionException
     * @expectedExceptionMessage I'm failing, free failing ...
     */
    public function all_discoverers_fail()
    {
        $chain = new DiscovererChain();
        $chain->addDiscoverer(new FailingDiscoverer());
        $chain->addDiscoverer(new FailingDiscoverer());

        $stream = $this->createEmptyStream();

        $chain->getVersion($stream);
    }

    /**
     * @return Stream
     */
    private function createEmptyStream()
    {
        return new Stream(fopen('php://temp', 'rw'));
    }
}

class SucceedingDiscoverer extends ChainableDiscoverer
{
    private $version;

    public function __construct(Version $version)
    {
        $this->version = $version;
    }

    protected function getVersionForStream(Stream $stream)
    {
        return $this->version;
    }
}

class FailingDiscoverer extends ChainableDiscoverer
{
    protected function getVersionForStream(Stream $stream)
    {
        throw new UnknownVersionException("I'm failing, free failing ...");
    }
}
