# pdfversion

[![Build Status](https://travis-ci.org/trekksoft/pdfversion.svg?branch=master)](https://travis-ci.org/trekksoft/pdfversion)
[![Code Coverage](https://scrutinizer-ci.com/g/trekksoft/pdfversion/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/trekksoft/pdfversion/?branch=master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/trekksoft/pdfversion/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/trekksoft/pdfversion/?branch=master)

Read the version of PDF files using PHP.

## Installation

    composer require "trekksoft/pdfversion":"~2.0"

## Examples

### Simple example

    <?php
    use Trekksoft\PdfVersion\File;
    use Trekksoft\PdfVersion\VersionDiscovery\HeaderCommentDiscoverer;
    use Trekksoft\PdfVersion\VersionDiscovery\UnknownVersionException;
    
    $file = new File('path/to/file.pdf');
    
    $discoverer = new HeaderCommentDiscoverer();
    
    try {
        $version = $discoverer->getVersion($file);
        echo $version->toString();
    } catch (UnknownVersionException $e) {
        echo $e->getMessage();
    }

### Trying multiple discoverers

    $file = new File('path/to/file.pdf');
    
    $discovererChain = new DiscovererChain([
        new HeaderCommentDiscoverer(),
        new YourOwnDiscoverer(),
        new SorryIhaventWrittenAnyOtherDiscoverers(),
    ]);
    
    $version = $discovererChain->getVersion($file);

### Using streams

    <?php
    $stream = new Stream($fileHandle);
    
    $discoverer = new HeaderCommentDiscoverer();
    $version = $discoverer->getVersion($stream);

## License

MIT. See LICENSE file.