<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion;

final class File implements StreamProvider
{
    /** @var string */
    private $path = '';

    /**
     * @param string $path
     *
     * @throws InvalidArgumentException
     */
    public function __construct($path)
    {
        if (!is_readable($path)) {
            throw new InvalidArgumentException("File '$path' must exist and be readable.");
        }

        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function getStream()
    {
        return new Stream(fopen($this->path, 'r'));
    }
}
