<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion;

final class Stream implements StreamProvider
{
    /** @var resource */
    private $handle;

    /**
     * @param resource $handle
     */
    public function __construct($handle)
    {
        $this->handle = $handle;
        $this->rewind();
    }

    public function rewind()
    {
        rewind($this->handle);
    }

    /**
     * @param int $numBytes
     *
     * @return string
     */
    public function readAndAdvance($numBytes)
    {
        return fread($this->handle, $numBytes);
    }

    /**
     * {@inheritdoc}
     */
    public function getStream()
    {
        return $this;
    }
}
