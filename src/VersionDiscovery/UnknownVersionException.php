<?php

/**
 * This file is part of pdfversion.
 *
 * (c) TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

use Trekksoft\PdfVersion\Exception;

class UnknownVersionException extends Exception
{
}
