<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

use Trekksoft\PdfVersion\StreamProvider;

interface Discoverer
{
    /**
     * @param StreamProvider $streamProvider
     *
     * @return Version
     *
     * @throws UnknownVersionException
     */
    public function getVersion(StreamProvider $streamProvider);
}
