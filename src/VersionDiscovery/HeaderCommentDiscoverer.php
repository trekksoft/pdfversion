<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

use Trekksoft\PdfVersion\Stream;

final class HeaderCommentDiscoverer extends ChainableDiscoverer
{
    /**
     * {@inheritdoc}
     */
    protected function getVersionForStream(Stream $stream)
    {
        $bytes = $stream->readAndAdvance(8);

        $matches = [];
        if (!preg_match('#%PDF-(\d)\.(\d)#', $bytes, $matches)) {
            throw new UnknownVersionException("Failed to find '%PDF-X.Y' header.");
        }

        return new Version($matches[1], $matches[2]);
    }
}
