<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

class Version
{
    /** @var int */
    private $major = 0;

    /** @var int */
    private $minor = 0;

    /**
     * @param int $major
     * @param int $minor
     */
    public function __construct($major, $minor)
    {
        $this->major = (int) $major;
        $this->minor = (int) $minor;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return sprintf('%d.%d', $this->major, $this->minor);
    }
}
