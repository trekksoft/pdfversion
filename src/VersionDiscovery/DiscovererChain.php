<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

use Trekksoft\PdfVersion\StreamProvider;

final class DiscovererChain implements Discoverer
{
    /** @var ChainableDiscoverer[] */
    private $discoverers = [];

    /**
     * @param ChainableDiscoverer[] $discoverers
     */
    public function __construct(array $discoverers = [])
    {
        foreach ($discoverers as $discoverer) {
            $this->addDiscoverer($discoverer);
        }
    }

    /**
     * @param ChainableDiscoverer $discoverer
     */
    public function addDiscoverer(ChainableDiscoverer $discoverer)
    {
        $this->discoverers[] = $discoverer;
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion(StreamProvider $streamProvider)
    {
        $stream = $streamProvider->getStream();

        if (empty($this->discoverers)) {
            throw new MissingDiscoverersException();
        }

        for ($i = 0, $c = count($this->discoverers); $i < $c; ++$i) {
            $discoverer = $this->discoverers[$i];
            $successor = $i + 1 == $c ? null : $this->discoverers[$i + 1];
            $discoverer->setSuccessor($successor);
        }

        return $this->discoverers[0]->getVersion($stream);
    }
}
