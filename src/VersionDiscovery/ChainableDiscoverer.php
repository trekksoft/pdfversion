<?php

/**
 * This file is part of pdfversion.
 *
 * (c) §TrekkSoft Ltd.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Trekksoft\PdfVersion\VersionDiscovery;

use Trekksoft\PdfVersion\Stream;
use Trekksoft\PdfVersion\StreamProvider;

/**
 * A chainable discoverer must not throw an UnknownVersionException in case it can't figure out the version,
 * but instead call the set successor. It must throw an UnknownVersionException in case no successor is set
 * and the version can't be determined.
 */
abstract class ChainableDiscoverer implements Discoverer
{
    /** @var Discoverer|null */
    private $successor;

    /**
     * @param Discoverer|null $discoverer
     */
    public function setSuccessor(Discoverer $discoverer = null)
    {
        $this->successor = $discoverer;
    }

    /**
     * @param Stream $stream
     *
     * @return Version
     *
     * @throws UnknownVersionException
     */
    abstract protected function getVersionForStream(Stream $stream);

    /**
     * {@inheritdoc}
     */
    public function getVersion(StreamProvider $streamProvider)
    {
        $stream = $streamProvider->getStream();

        try {
            return $this->getVersionForStream($stream);
        } catch (UnknownVersionException $e) {
            if ($this->successor) {
                return $this->successor->getVersion($stream);
            }

            throw $e;
        }
    }
}
